# Local Environment
__PHP Version__: 7.4 and below \
__PHP Framework__: Codeigniter 3.x \
__Composer Version__: Not required \
__Additional PHP Extension__:
- Imagick
- cURL

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Unreleased
## [2.0.0] - 2022-01-17
### Added
- Add __order__ table
  - __order_id__ INT 11 (auto_increment) (primary)
  - __user_id__ INT 11
  - __food_id__ VARCHAR 255

### Changed
- Added new column to __merchant__ table
  - __created_date__ date current_timestamp

### Removed
- Removed soft delete function

### Fixed
- Fixed SSL issue using htaccess

---

# Released
## [1.1.1] - 2022-01-13
### Security
- CSRF token 

### Fixed
- Login button alignment

## [1.1.0] - 2022-01-12
### Added
- Permission module
  - Added __permission__ table
    - __id__ INT 11 (auto increment) (primary)
    - __name__  VARCHAR 255
    - __allowed_access__ VARCHAR 255

### Deprecated
- Soft delete function

## [1.0.0] - 2022-01-12
### Added
- Added login screen UI
- Integrated with login UI
- Added __user__ table
  - __id__ INT 11 (auto increment) (primary)
  - __name__ VARCHAR 255
  - __email__ VARCHAR 255
- Added __merchant__ table
  - __id__ INT 11 (auto increment) (primary)
  - __name__ VARCHAR 255
  - __email__ VARCHAR 255
